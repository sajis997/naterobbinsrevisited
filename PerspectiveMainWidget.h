#ifndef PERSPECTIVEMAINWIDGET_H
#define PERSPECTIVEMAINWIDGET_H

#include <QWidget>
#include "ui_perspective.h"

class SharedWidgetData;

class PerspectiveMainWidget : public QWidget , private Ui::PerspectiveWidget
{
    Q_OBJECT
public:
    PerspectiveMainWidget(QWidget *parent = 0);
    ~PerspectiveMainWidget();

    void setSharedDataInstance(SharedWidgetData*);

    SharedWidgetData *getSharedDataInstance() const
    {
        return pSharedData;
    }




private:

    void createConnections();

    SharedWidgetData *pSharedData;

};

#endif // PERSPECTIVEMAINWIDGET_H
