#include "FrustumMainWidget.h"

#include "SharedWidgetData.h"

FrustumMainWidget::FrustumMainWidget(QWidget *parent)
    :QWidget(parent)
{
    setupUi(this);


    //set up some threshold value for the label
    leftValueLabelFrustum->setMinValue(-10);
    leftValueLabelFrustum->setMaxValue(10);
    leftValueLabelFrustum->setStepValue(0.1);


    rightValueLabelFrustum->setMinValue(-10);
    rightValueLabelFrustum->setMaxValue(10);
    rightValueLabelFrustum->setStepValue(0.1);


    bottomValueLabelFrustum->setMinValue(-10);
    bottomValueLabelFrustum->setMaxValue(10);
    bottomValueLabelFrustum->setStepValue(0.1);


    topValueLabelFrustum->setMinValue(-10);
    topValueLabelFrustum->setMaxValue(10);
    topValueLabelFrustum->setStepValue(0.1);


    nearValueLabelFrustum->setMinValue(-5.0);
    nearValueLabelFrustum->setMaxValue(5.0);
    nearValueLabelFrustum->setStepValue(0.01);


    farValueLabelFrustum->setMinValue(-5.0);
    farValueLabelFrustum->setMaxValue(5.0);
    farValueLabelFrustum->setStepValue(0.01);

    eyeXPositionValueLabel->setMinValue(-5.0);
    eyeXPositionValueLabel->setMaxValue(5.0);
    eyeXPositionValueLabel->setStepValue(0.1);

    eyeYPositionValueLabel->setMinValue(-5.0);
    eyeYPositionValueLabel->setMaxValue(5.0);
    eyeYPositionValueLabel->setStepValue(0.1);

    eyeZPositionValueLabel->setMinValue(-5.0);
    eyeZPositionValueLabel->setMaxValue(5.0);
    eyeZPositionValueLabel->setStepValue(0.1);

    lookAtXPositionValueLabel->setMinValue(-5.0);
    lookAtXPositionValueLabel->setMaxValue(5.0);
    lookAtXPositionValueLabel->setStepValue(0.1);

    lookAtYPositionValueLabel->setMinValue(-5.0);
    lookAtYPositionValueLabel->setMaxValue(5.0);
    lookAtYPositionValueLabel->setStepValue(0.1);

    lookAtZPositionValueLabel->setMinValue(-5.0);
    lookAtZPositionValueLabel->setMaxValue(5.0);
    lookAtZPositionValueLabel->setStepValue(0.1);


    upVectorXPositionValueLabel->setMinValue(-2.0);
    upVectorXPositionValueLabel->setMaxValue(2.0);
    upVectorXPositionValueLabel->setStepValue(0.1);


    upVectorYPositionValueLabel->setMinValue(-2.0);
    upVectorYPositionValueLabel->setMaxValue(2.0);
    upVectorYPositionValueLabel->setStepValue(0.1);


    upVectorZPositionValueLabel->setMinValue(-2.0);
    upVectorZPositionValueLabel->setMaxValue(2.0);
    upVectorZPositionValueLabel->setStepValue(0.1);
}

FrustumMainWidget::~FrustumMainWidget()
{

}


void FrustumMainWidget::setSharedDataInstance(SharedWidgetData *data)
{
    if(data) {
        pSharedData = data;
        createConnections();
    }
}


void FrustumMainWidget::createConnections()
{
    connect(leftValueLabelFrustum,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setFrustumLeft(float)));
    connect(rightValueLabelFrustum,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setFrustumRight(float)));
    connect(bottomValueLabelFrustum,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setFrustumBottom(float)));
    connect(topValueLabelFrustum,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setFrustumTop(float)));
    connect(nearValueLabelFrustum,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setFrustumNear(float)));
    connect(farValueLabelFrustum,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setFrustumFar(float)));

    connect(eyeXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraXPos(float)));
    connect(eyeYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraYPos(float)));
    connect(eyeZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraZPos(float)));

    connect(lookAtXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtXPos(float)));
    connect(lookAtYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtYPos(float)));
    connect(lookAtZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtZPos(float)));

    connect(upVectorXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorXfactor(float)));
    connect(upVectorYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorYfactor(float)));
    connect(upVectorZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorZfactor(float)));
}
