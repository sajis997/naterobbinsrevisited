#include <QtGui>
#include "ProjectionMainWindow.h"
#include "singleton.h"
#include "SharedWidgetData.h"


ProjectionMainWindow::ProjectionMainWindow(QWidget *parent)
    :QMainWindow(parent)
{
    setupUi(this);


    //create the 3 separate widget and arrange them inside the stack layout
    //and it is children of the scroll area contents
    stackViewWidgetFrustum = new FrustumMainWidget( scrollAreaWidgetContents_5);
    stackViewWidgetOrtho = new OrthoMainWidget( scrollAreaWidgetContents_5 );
    stackViewWidgetPerspective = new PerspectiveMainWidget( scrollAreaWidgetContents_5);

    //you do not need to send the parent as argument
    stackLayout = new QStackedLayout();

    //arrange the widget inside the layout
    stackLayout->addWidget(stackViewWidgetPerspective);
    stackLayout->addWidget(stackViewWidgetFrustum);
    stackLayout->addWidget(stackViewWidgetOrtho);


    //instantiate a grid layout and add the widgets later to it
    gridLayout = new QGridLayout(scrollAreaWidgetContents_5);

    verticalLayoutCustom = new QVBoxLayout();
    verticalLayout->addStretch();

    horizontalLayoutCustom = new QHBoxLayout();
    horizontalLayoutCustom->addStretch();
    horizontalLayoutCustom->addLayout(stackLayout);
    horizontalLayoutCustom->addStretch();



    verticalLayoutCustom->addLayout(horizontalLayoutCustom);
    verticalLayoutCustom->addStretch();

    gridLayout->addLayout(verticalLayoutCustom,0,0,1,1);


    Singleton<SharedWidgetData>::init(new SharedWidgetData());

    WorldSpaceViewWidget->setSharedDataInstance(Singleton<SharedWidgetData>::getPtr());
    ScreenSpaceViewWidget->setSharedDataInstance(Singleton<SharedWidgetData>::getPtr());
    CommandManipulationGroupBox->setSharedDataInstanceGroupBox(Singleton<SharedWidgetData>::getPtr());
    stackViewWidgetOrtho->setSharedDataInstance(Singleton<SharedWidgetData>::getPtr());
    stackViewWidgetPerspective->setSharedDataInstance(Singleton<SharedWidgetData>::getPtr());
    stackViewWidgetFrustum->setSharedDataInstance(Singleton<SharedWidgetData>::getPtr());


    SharedWidgetData *pSharedData = Singleton<SharedWidgetData>::getPtr();

    //shared data changing will change the widget in the
    //stack layout and the glwidgets will be updated accordingly
    connect(pSharedData,SIGNAL(projectionModeChanged(int)),stackLayout,SLOT(setCurrentIndex(int)));

    //where the following signal comes from
    connect(pSharedData,SIGNAL(projectionModeChanged()),this,SLOT(updateGLWidgets()));

    //CONNECT THE left  value label ortho
    connect(pSharedData,SIGNAL(leftOrthoFrameChanged()),this,SLOT(updateGLWidgets()));
    //connect the right value label ortho
    connect(pSharedData,SIGNAL(rightOrthoFrameChanged()),this,SLOT(updateGLWidgets()));
    //connect the bottom value label
    connect(pSharedData,SIGNAL(bottomOrthoFrameChanged()),this,SLOT(updateGLWidgets()));
    //cconect the top value label
    connect(pSharedData,SIGNAL(topOrthoFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(nearOrthoFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(farOrthoFrameChanged()),this,SLOT(updateGLWidgets()));


    connect(pSharedData,SIGNAL(cameraXPosChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(cameraYPosChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(cameraZPosChanged()),this,SLOT(updateGLWidgets()));

    connect(pSharedData,SIGNAL(lookAtXPosChanged()),this,SLOT(updateGLWidgets())),
    connect(pSharedData,SIGNAL(lookAtYPosChanged()),this,SLOT(updateGLWidgets())),
    connect(pSharedData,SIGNAL(lookAtZPosChanged()),this,SLOT(updateGLWidgets())),

    connect(pSharedData,SIGNAL(upVectorXfactorChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(upVectorYfactorChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(upVectorZfactorChanged()),this,SLOT(updateGLWidgets()));

    connect(pSharedData,SIGNAL(aspectPerspectiveFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(fovPerspectiveFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(nearPerspectiveFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(farPerspectiveFrameChanged()),this,SLOT(updateGLWidgets()));


    connect(pSharedData,SIGNAL(leftFrustumFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(rightFrustumFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(bottomFrustumFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(topFrustumFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(nearFrustumFrameChanged()),this,SLOT(updateGLWidgets()));
    connect(pSharedData,SIGNAL(farFrustumFrameChanged()),this,SLOT(updateGLWidgets()));


    connect(ScreenSpaceViewWidget,SIGNAL(projectionScreenMatrixChanged()),this,SLOT(updateProjectionMatrixLabelValues()));
    connect(ScreenSpaceViewWidget,SIGNAL(modelMatrixScreenSpaceChanged()),this,SLOT(updateModelMatrixLabelValues()));
    connect(ScreenSpaceViewWidget,SIGNAL(viewMatrixScreenSpaceChanged()),this,SLOT(updateViewMatrixLabelValues()));
    connect(ScreenSpaceViewWidget,SIGNAL(modelviewScreenMatrixChanged()),this,SLOT(updateModelViewMatrixLabelValues()));
}


void ProjectionMainWindow::updateProjectionMatrixLabelValues()
{
    SharedWidgetData *pSharedData = Singleton<SharedWidgetData>::getPtr();


    projectionLabelElement11->setNum(pSharedData->projectionMatrix[0]);
    projectionLabelElement12->setNum(pSharedData->projectionMatrix[4]);
    projectionLabelElement13->setNum(pSharedData->projectionMatrix[8]);
    projectionLabelElement14->setNum(pSharedData->projectionMatrix[12]);

    projectionLabelElement21->setNum(pSharedData->projectionMatrix[1]);
    projectionLabelElement22->setNum(pSharedData->projectionMatrix[5]);
    projectionLabelElement23->setNum(pSharedData->projectionMatrix[9]);
    projectionLabelElement24->setNum(pSharedData->projectionMatrix[13]);


    projectionLabelElement31->setNum(pSharedData->projectionMatrix[2]);
    projectionLabelElement32->setNum(pSharedData->projectionMatrix[6]);
    projectionLabelElement33->setNum(pSharedData->projectionMatrix[10]);
    projectionLabelElement34->setNum(pSharedData->projectionMatrix[14]);


    projectionLabelElement41->setNum(pSharedData->projectionMatrix[3]);
    projectionLabelElement42->setNum(pSharedData->projectionMatrix[7]);
    projectionLabelElement43->setNum(pSharedData->projectionMatrix[11]);
    projectionLabelElement44->setNum(pSharedData->projectionMatrix[15]);

}

void ProjectionMainWindow::updateModelMatrixLabelValues()
{
    SharedWidgetData *pSharedData = Singleton<SharedWidgetData>::getPtr();

    modelLabelElement11->setNum(pSharedData->modelMatrix[0]);
    modelLabelElement12->setNum(pSharedData->modelMatrix[4]);
    modelLabelElement13->setNum(pSharedData->modelMatrix[8]);
    modelLabelElement14->setNum(pSharedData->modelMatrix[12]);

    modelLabelElement21->setNum(pSharedData->modelMatrix[1]);
    modelLabelElement22->setNum(pSharedData->modelMatrix[5]);
    modelLabelElement23->setNum(pSharedData->modelMatrix[9]);
    modelLabelElement24->setNum(pSharedData->modelMatrix[13]);


    modelLabelElement31->setNum(pSharedData->modelMatrix[2]);
    modelLabelElement32->setNum(pSharedData->modelMatrix[6]);
    modelLabelElement33->setNum(pSharedData->modelMatrix[10]);
    modelLabelElement34->setNum(pSharedData->modelMatrix[14]);

    modelLabelElement41->setNum(pSharedData->modelMatrix[3]);
    modelLabelElement42->setNum(pSharedData->modelMatrix[7]);
    modelLabelElement43->setNum(pSharedData->modelMatrix[11]);
    modelLabelElement44->setNum(pSharedData->modelMatrix[15]);
}

void ProjectionMainWindow::updateViewMatrixLabelValues()
{
    SharedWidgetData *pSharedData = Singleton<SharedWidgetData>::getPtr();

    viewLabelElement11->setNum(pSharedData->viewMatrix[0]);
    viewLabelElement12->setNum(pSharedData->viewMatrix[4]);
    viewLabelElement13->setNum(pSharedData->viewMatrix[8]);
    viewLabelElement14->setNum(pSharedData->viewMatrix[12]);

    viewLabelElement21->setNum(pSharedData->viewMatrix[1]);
    viewLabelElement22->setNum(pSharedData->viewMatrix[5]);
    viewLabelElement23->setNum(pSharedData->viewMatrix[9]);
    viewLabelElement24->setNum(pSharedData->viewMatrix[13]);

    viewLabelElement31->setNum(pSharedData->viewMatrix[2]);
    viewLabelElement32->setNum(pSharedData->viewMatrix[6]);
    viewLabelElement33->setNum(pSharedData->viewMatrix[10]);
    viewLabelElement34->setNum(pSharedData->viewMatrix[14]);


    viewLabelElement41->setNum(pSharedData->viewMatrix[3]);
    viewLabelElement42->setNum(pSharedData->viewMatrix[7]);
    viewLabelElement43->setNum(pSharedData->viewMatrix[11]);
    viewLabelElement44->setNum(pSharedData->viewMatrix[15]);
}

void ProjectionMainWindow::updateModelViewMatrixLabelValues()
{
    SharedWidgetData *pSharedData = Singleton<SharedWidgetData>::getPtr();

    modelViewLabelElement11->setNum(pSharedData->modelViewMatrix[0]);
    modelViewLabelElement12->setNum(pSharedData->modelViewMatrix[4]);
    modelViewLabelElement13->setNum(pSharedData->modelViewMatrix[8]);
    modelViewLabelElement14->setNum(pSharedData->modelViewMatrix[12]);


    modelViewLabelElement21->setNum(pSharedData->modelViewMatrix[1]);
    modelViewLabelElement22->setNum(pSharedData->modelViewMatrix[5]);
    modelViewLabelElement23->setNum(pSharedData->modelViewMatrix[9]);
    modelViewLabelElement24->setNum(pSharedData->modelViewMatrix[13]);


    modelViewLabelElement31->setNum(pSharedData->modelViewMatrix[2]);
    modelViewLabelElement32->setNum(pSharedData->modelViewMatrix[6]);
    modelViewLabelElement33->setNum(pSharedData->modelViewMatrix[10]);
    modelViewLabelElement34->setNum(pSharedData->modelViewMatrix[14]);


    modelViewLabelElement41->setNum(pSharedData->modelViewMatrix[3]);
    modelViewLabelElement42->setNum(pSharedData->modelViewMatrix[7]);
    modelViewLabelElement43->setNum(pSharedData->modelViewMatrix[11]);
    modelViewLabelElement44->setNum(pSharedData->modelViewMatrix[15]);
}



void ProjectionMainWindow::updateGLWidgets()
{
    ScreenSpaceViewWidget->screenReshape(ScreenSpaceViewWidget->getScreenWidgetSpaceWidth(),ScreenSpaceViewWidget->getScreenWidgetSpaceHeight());
    ScreenSpaceViewWidget->updateGL();
    WorldSpaceViewWidget->worldReshape(WorldSpaceViewWidget->getWorldWidgetSpaceWidth(),WorldSpaceViewWidget->getWorldWidgetSpaceHeight());
    WorldSpaceViewWidget->updateGL();
}


ProjectionMainWindow::~ProjectionMainWindow()
{

}
