#include <QDebug>
#include "SharedWidgetData.h"




SharedWidgetData::SharedWidgetData( QObject *parent)
    : QObject(parent)
{
    //make all  the matrices
    //the identity matrices
    identity(modelViewMatrix);
    identity(projectionMatrix);
    identity(invertedMatrix);


    identity(modelMatrix);
    identity(viewMatrix);

    world_draw = true;

    swapped = false;

    resetParameters = false;

    model = NULL;


    //by default set a file path so
    //that the model can be loaded
    //initially
    filePath = "data/porsche.obj";


    //read  the xml file to read more configuration
//    dataReader = new XmlStreamReader(&cellMap);

//    if(!dataReader->readFile("config.xml"))
//        qDebug() << "File could not be processes";

    //intialize the positional and orientational vectors
    translationVector[0] = 0.0;
    translationVector[1] = 0.0;
    translationVector[2] = 0.0;


    rotationVector[0] = 0.0;
    rotationVector[1] = 0.0;
    rotationVector[2] = 1.0;
    rotationVector[3] = 0.0;


    scaleVector[0] = 1.0;
    scaleVector[1] = 1.0;
    scaleVector[2] = 1.0;


    //initiate the mode
    //with the perspective
    mode = PERSPECTIVE;

    //initiate the camera position, look at position and the up vector
    cameraPos[0] = 0.0;
    cameraPos[1] = 0.0;
    cameraPos[2] = 2.0;

    lookAt[0] = 0.0;
    lookAt[1] = 0.0;
    lookAt[2] = 0.0;

    upVector[0] = 0.0;
    upVector[1] = 1.0;
    upVector[2] = 0.0;


    //intialize the frame values
    perspectiveFrame[0] = 60.0;
    perspectiveFrame[1] = 1.0;
    perspectiveFrame[2] = 1.0;
    perspectiveFrame[3] = 10.0;

    orthoFrame[0] = -1.0;
    orthoFrame[1] = 1.0;
    orthoFrame[2] = -1.0;
    orthoFrame[3] = 1.0;
    orthoFrame[4] = 1.0;
    orthoFrame[5] = 3.5;

    frustumFrame[0] = -1.0;
    frustumFrame[1] = 1.0;
    frustumFrame[2] = -1.0;
    frustumFrame[3] = 1.0;
    frustumFrame[4] = 1.0;
    frustumFrame[5] = 3.5;
}


SharedWidgetData::~SharedWidgetData()
{
    if(model) {
        glmDelete(model);
        model = NULL;

    }
}


void SharedWidgetData::setTranslationXFactor(float xFactor)
{
    translationVector[0] = xFactor;

    emit translationXFactorChanged();
}

void SharedWidgetData::setTranslationYFactor(float yFactor)
{
    translationVector[1] = yFactor;

    emit translationYFactorChanged();
}

void SharedWidgetData::setTranslationZFactor(float zFactor)
{
    translationVector[2] = zFactor;

    emit translationZFactorChanged();
}

void SharedWidgetData::setRotationAngleFactor(float angle)
{
    rotationVector[0] = angle;

    emit rotationAngleFactorChanged();

    //qDebug() << rotationVector[0] << " " << "rotation angle changed";
}

void SharedWidgetData::setRotationXFactor(float xRotationFactor)
{
    rotationVector[1] = xRotationFactor;

    emit rotationXFactorChanged();

    //qDebug() << rotationVector[1] << " " << "rotation X vector changed";
}

void SharedWidgetData::setRotationYFactor(float yRotationFactor)
{
    rotationVector[2] = yRotationFactor;

    emit rotationYFactorChanged();

    //qDebug() << rotationVector[2] << " " << "rotation Y vector changed";
}

void SharedWidgetData::setRotationZFactor(float zRotationFactor)
{
    rotationVector[3] = zRotationFactor;

    emit rotationZFactorChanged();

    //qDebug() << rotationVector[3] << " " << "rotation Z vector changed";
}

void SharedWidgetData::setScaleXFactor(float scaleXFactor)
{
    scaleVector[0] = scaleXFactor;

    emit scaleXFactorChanged();
}

void SharedWidgetData::setScaleYFactor(float scaleYFactor)
{
    scaleVector[1] = scaleYFactor;

    emit scaleYFactorChanged();
}

void SharedWidgetData::setScaleZFactor(float scaleZFactor)
{
    scaleVector[2] = scaleZFactor;

    emit scaleZFactorChanged();
}

void SharedWidgetData::setPerspectiveFOV(float fov)
{
    perspectiveFrame[0] = fov;

    emit fovPerspectiveFrameChanged();
}

void SharedWidgetData::setPerspectiveAspect(float aspect)
{
    perspectiveFrame[1] = aspect;

    emit aspectPerspectiveFrameChanged();
}

void SharedWidgetData::setPerspectiveNear(float near)
{
    perspectiveFrame[2] = near;

    emit nearPerspectiveFrameChanged();
}

void SharedWidgetData::setPerspectiveFar(float far)
{
    perspectiveFrame[3] = far;

    emit farPerspectiveFrameChanged();
}

void SharedWidgetData::setOrthoLeft(float left)
{
    orthoFrame[0] = left;

    emit leftOrthoFrameChanged();
}

void SharedWidgetData::setOrthoRight(float right)
{
    orthoFrame[1] = right;

    emit rightOrthoFrameChanged();
}

void SharedWidgetData::setOrthoBottom(float bottom)
{
    orthoFrame[2] = bottom;

    emit bottomOrthoFrameChanged();
}

void SharedWidgetData::setOrthoTop(float top)
{
    orthoFrame[3] = top;

    emit topOrthoFrameChanged();
}

void SharedWidgetData::setOrthoNear(float near)
{
    orthoFrame[4] = near;

    emit nearOrthoFrameChanged();
}

void SharedWidgetData::setOrthoFar(float far)
{
    orthoFrame[5] = far;

    emit farOrthoFrameChanged();
}


void SharedWidgetData::setFrustumLeft(float left)
{
    frustumFrame[0] = left;


    emit leftFrustumFrameChanged();
}

void SharedWidgetData::setFrustumRight(float right)
{
    frustumFrame[1] = right;

    emit rightFrustumFrameChanged();
}

void SharedWidgetData::setFrustumBottom(float bottom)
{
    frustumFrame[2] = bottom;

    emit bottomFrustumFrameChanged();
}

void SharedWidgetData::setFrustumTop(float top)
{
    frustumFrame[3] = top;

    emit topFrustumFrameChanged();
}

void SharedWidgetData::setFrustumNear(float near)
{
    frustumFrame[4] = near;

    emit nearFrustumFrameChanged();
}

void SharedWidgetData::setFrustumFar(float far)
{
    frustumFrame[5] = far;

    emit farFrustumFrameChanged();
}


void SharedWidgetData::setModelViewMatrix(const GLfloat *modelview)
{
    for(int i = 0; i < 16; i++)
        modelViewMatrix[i] = modelview[i];
}

void SharedWidgetData::setProjectionMatrix(const GLfloat *projection)
{
    for(int i = 0; i < 16; i++)
        projectionMatrix[i] = projection[i];
}

void SharedWidgetData::setInverseMatrix(const GLfloat *inversion)
{
    for(int i = 0; i < 16; i++)
        invertedMatrix[i] = inversion[i];
}


void SharedWidgetData::setSwapFlag(bool swap)
{
    swapped = swap;
}


void SharedWidgetData::setWorldDrawFlag(bool worldDraw)
{
    world_draw = worldDraw;
}

void SharedWidgetData::setResetParametesFlag()
{
    resetParameters = true;

    //qDebug() << "reset paremeters set";

    emit parametersToBeReset();
}


void SharedWidgetData::setCameraXPos(float x)
{
    cameraPos[0] = x;

    emit cameraXPosChanged();
}

void SharedWidgetData::setCameraYPos(float y)
{
    cameraPos[1] = y;

    emit cameraYPosChanged();
}

void SharedWidgetData::setCameraZPos(float z)
{
    cameraPos[2] = z;

    emit cameraZPosChanged();
}

void SharedWidgetData::setLookAtXPos(float x)
{
    lookAt[0] = x;

    emit lookAtXPosChanged();
}

void SharedWidgetData::setLookAtYPos(float y )
{
    lookAt[1] = y;

    emit lookAtYPosChanged();
}

void SharedWidgetData::setLookAtZPos(float z )
{
    lookAt[2] = z;

    emit lookAtZPosChanged();
}


void SharedWidgetData::setUpVectorXfactor(float x)
{
    upVector[0] = x;

    emit upVectorXfactorChanged();
}

void SharedWidgetData::setUpVectorYfactor(float y)
{
    upVector[1] = y;

    emit upVectorYfactorChanged();
}

void SharedWidgetData::setUpVectorZfactor(float z)
{
    upVector[2] = z;

    emit upVectorZfactorChanged();
}


void SharedWidgetData::setOrthoMode()
{
    mode = ORTHO;

    int modeIndex = mode;

    //first signal is emitted to change the stack widget through the index value
    emit projectionModeChanged(modeIndex);

    //second signal is emitted to make the corresponding changes to both the widgets
    emit projectionModeChanged();
}

void SharedWidgetData::setPerspectiveMode()
{
    mode = PERSPECTIVE;

    int modeIndex = mode;

    //emit signal to change the stack layout
    emit projectionModeChanged(modeIndex);
    //emit signal to update the glwidgets
    emit projectionModeChanged();
}

void SharedWidgetData::setFrustumMode()
{
    mode = FRUSTUM;

    int modeIndex = mode;

    emit projectionModeChanged(modeIndex);
    emit projectionModeChanged();
}


void SharedWidgetData::drawModel(const QString &file)
{
    model = glmReadOBJ(const_cast<char*>(file.toStdString().c_str()));

    if (!model) exit(0);

    glmUnitize(model);
    glmFacetNormals(model);
    glmVertexNormals(model, 90.0);


    glmDraw(model, GLM_SMOOTH | GLM_MATERIAL);

    //qDebug() << "Model is redrawn";
}


void SharedWidgetData::printModelViewMatrix() const
{
    qDebug() << "Printing the model view matrix";
    qDebug() << modelViewMatrix[0] << " " <<  modelViewMatrix[4] << " " << modelViewMatrix[8]  << " " <<  modelViewMatrix[12];
    qDebug() << modelViewMatrix[1] << " " <<  modelViewMatrix[5] << " " << modelViewMatrix[9]  << " " <<  modelViewMatrix[13];
    qDebug() << modelViewMatrix[2] << " " <<  modelViewMatrix[6] << " " << modelViewMatrix[10] << " " <<  modelViewMatrix[14];
    qDebug() << modelViewMatrix[3] << " " <<  modelViewMatrix[7] << " " << modelViewMatrix[11] << " " <<  modelViewMatrix[15];
}

void SharedWidgetData::printProjectionMatrix() const
{
    qDebug() << "Printing the projection matrix";
    qDebug() << projectionMatrix[0] << " " << projectionMatrix[4] << " " << projectionMatrix[8] << " " << projectionMatrix[12];
    qDebug() << projectionMatrix[1] << " " << projectionMatrix[5] << " " << projectionMatrix[9] << " " << projectionMatrix[13];
    qDebug() << projectionMatrix[2] << " " << projectionMatrix[6] << " " << projectionMatrix[10] << " " << projectionMatrix[14];
    qDebug() << projectionMatrix[3] << " " << projectionMatrix[7] << " " << projectionMatrix[11] << " " << projectionMatrix[15];
}

void SharedWidgetData::printInverseMatrix() const
{
    qDebug() << "Printing the inversion matrix";
    qDebug() << invertedMatrix[0] << " " << invertedMatrix[4] << " " << invertedMatrix[8] << " " << invertedMatrix[12];
    qDebug() << invertedMatrix[1] << " " << invertedMatrix[5] << " " << invertedMatrix[9] << " " << invertedMatrix[13];
    qDebug() << invertedMatrix[2] << " " << invertedMatrix[6] << " " << invertedMatrix[10] << " " << invertedMatrix[14];
    qDebug() << invertedMatrix[3] << " " << invertedMatrix[7] << " " << invertedMatrix[11] << " " << invertedMatrix[15];
}


void SharedWidgetData::printViewMatrix() const
{
    qDebug() << "Printing the view matrix";
    qDebug() << viewMatrix[0] << " " << viewMatrix[4] << " " << viewMatrix[8] << " " << viewMatrix[12];
    qDebug() << viewMatrix[1] << " " << viewMatrix[5] << " " << viewMatrix[9] << " " << viewMatrix[13];
    qDebug() << viewMatrix[2] << " " << viewMatrix[6] << " " << viewMatrix[10] << " " << viewMatrix[14];
    qDebug() << viewMatrix[3] << " " << viewMatrix[7] << " " << viewMatrix[11] << " " << viewMatrix[15];
}
