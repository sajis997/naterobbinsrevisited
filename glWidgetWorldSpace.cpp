#include <QtGui>
#include <QtOpenGL>



#include "glWidgetWorldSpace.h"
#include "SharedWidgetData.h"

GLWidgetWorldSpace::GLWidgetWorldSpace(QWidget *parent)
    : QGLWidget(parent),xRot(0),yRot(0),zRot(0)
{
    setFormat(QGL::DoubleBuffer | QGL::DepthBuffer);

    pSharedData = NULL;

    worldWidgetSpaceHeight = 0;
    worldWidgetSpaceWidth = 0;
}


void GLWidgetWorldSpace::setSharedDataInstance(SharedWidgetData *sharedData)
{
    if( sharedData)
        pSharedData = sharedData;
    else
        qDebug() << "Shared data not instantiated in world space";

    createActions();
}



void GLWidgetWorldSpace::initializeGL()
{

    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    //glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);



}

void GLWidgetWorldSpace::createActions()
{
    resetWorldAction = new QAction(tr("&Reset the world scene..."),this);
    resetWorldAction->setStatusTip(tr("Reset the world scene"));

    connect(resetWorldAction,SIGNAL(triggered()),this,SLOT(resetWorldScene()));
}

void GLWidgetWorldSpace::resetWorldScene()
{
    xRot = 0;
    yRot = 0;
    zRot = 0;
}


void GLWidgetWorldSpace::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

    menu.addAction(resetWorldAction);

    menu.exec(event->globalPos());
}

void GLWidgetWorldSpace::worldReshape(int width, int height)
{
    makeCurrent();

    //set the viewport - take the whole space in the  viewport
    glViewport(0,0,width,height);

    //change to the projection matrix mode
    glMatrixMode(GL_PROJECTION);
    //make the projection matrix - the identity one
    glLoadIdentity();

    //set the perspective value to the projection matrix
    gluPerspective(60.0,(GLfloat)width/height,0.01,256.0);

    //SAVE THE projection matrix to  the shared  widgets data
    //glGetDoublev(GL_PROJECTION_MATRIX,pSharedData->projectionMatrix);

    //change to the model view matrix
    glMatrixMode(GL_MODELVIEW);

    //make the model view matrix - an identity one
    glLoadIdentity();

    //translate the model to the negative z-axis
    glTranslatef(0.0,0.0,-5.0);
    glRotatef(-45.0,0.0,1.0,0.0);

    //eventually the model view matrix is getting changed


    //save the model view translation into the shared model-view matrix
    //glGetDoublev(GL_MODELVIEW_MATRIX,pSharedData->modelViewMatrix);

    glClearColor(0.0,0.0,0.0,0.0);

    //may have been better if we had them inside the initiaize function
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
}


void GLWidgetWorldSpace::resizeGL(int width,int height)
{

    //initialize the private variable to the new width and height
    worldWidgetSpaceHeight = height;
    worldWidgetSpaceWidth = width;

    worldReshape(worldWidgetSpaceWidth,worldWidgetSpaceHeight);
}

void GLWidgetWorldSpace::drawAxes()
{
    //red color
    glColor3ub(255, 0, 0);

    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(1.0, 0.0, 0.0);
    glVertex3f(0.75, 0.25, 0.0);
    glVertex3f(0.75, -0.25, 0.0);
    glVertex3f(1.0, 0.0, 0.0);
    glVertex3f(0.75, 0.0, 0.25);
    glVertex3f(0.75, 0.0, -0.25);
    glVertex3f(1.0, 0.0, 0.0);
    glEnd();

    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, 0.75, 0.25);
    glVertex3f(0.0, 0.75, -0.25);
    glVertex3f(0.0, 1.0, 0.0);
    glVertex3f(0.25, 0.75, 0.0);
    glVertex3f(-0.25, 0.75, 0.0);
    glVertex3f(0.0, 1.0, 0.0);
    glEnd();

    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 1.0);
    glVertex3f(0.25, 0.0, 0.75);
    glVertex3f(-0.25, 0.0, 0.75);
    glVertex3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.25, 0.75);
    glVertex3f(0.0, -0.25, 0.75);
    glVertex3f(0.0, 0.0, 1.0);
    glEnd();

    glColor3ub(255, 255, 0);

    //set  the font of the text to be written
    setFont(QFont("Times",12));

    renderText(1.1, 0.0, 0.0,QChar('x'));


    renderText(0.0, 1.1, 0.0,QChar('y'));


    renderText(0.0,0.0,1.1,QChar('z'));

}


void GLWidgetWorldSpace::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void GLWidgetWorldSpace::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(xRot + 8 * dy);
        setYRotation(yRot + 8 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(xRot + 8 * dy);
        setZRotation(zRot + 8 * dx);
    }
    lastPos = event->pos();
}

void GLWidgetWorldSpace::normalizeAngle(int *angle)
{
    while(*angle < 0)
        *angle += 360 * 16;
    while(*angle > 360 * 16)
        *angle -= 360 * 16;
}


void GLWidgetWorldSpace::setXRotation(int angle)
{
    normalizeAngle(&angle);

    if(angle != xRot) {
        xRot = angle;
        emit xRotationChanged(angle);
        updateGL();
    }
}

void GLWidgetWorldSpace::setYRotation(int angle)
{
    normalizeAngle(&angle);

    if(angle != yRot) {
        yRot = angle;
        emit yRotationChanged(angle);
        updateGL();
    }
}

void GLWidgetWorldSpace::setZRotation(int angle)
{
    normalizeAngle(&angle);

    if(angle != zRot) {
        zRot = angle;
        emit zRotationChanged(angle);
        updateGL();
    }
}


void GLWidgetWorldSpace::paintGL()
{
    //the matrix mode is model view now


    //initialize the light position
    GLfloat pos[] = { 0.0, 0.0, 1.0, 0.0 };

    //variable to store the length between eye and the look at position
    double length;

    //the vector from the eye to the look at position
    float l[3];

    //vector is calculated
    l[0] = pSharedData->lookAt[0] - pSharedData->cameraPos[0];
    l[1] = pSharedData->lookAt[1] - pSharedData->cameraPos[1];
    l[2] = pSharedData->lookAt[2] - pSharedData->cameraPos[2];

    //normalize the vector
    length = normalize(l);


    //get the inversion of the model-view matrix that have been saved in the resizeGL()
    bool inverted = invert(pSharedData->modelViewMatrix, pSharedData->invertedMatrix);


    //clear both the color buffer and depth buffer - for the hidden surface removal
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //scene rotation
    glPushMatrix();
    glRotatef(xRot / 16.0,1.0,0.0,0.0);
    glRotatef(yRot / 16.0,0.0,1.0,0.0);
    //glRotatef(zRot / 16.0,0.0,0.0,1.0);



    if (pSharedData->world_draw) {

        //if the world draw is enabled  we enable the lightig
        glEnable(GL_LIGHTING);

        //during the resize event the model view matrix was set and we push that current matrix onto the stack
        //the resize event is called when the window is opened for the first time
        glPushMatrix();

        //multiply the inverse matrix to the current model view matrix
        //the current model view matrix gets changed
        //by this we get  the virtual camera location - the local camera coordinates
        glMultMatrixf(pSharedData->invertedMatrix);

        //save that current model view matrix
        //to the shared widget data
        //glGetDoublev(GL_MODELVIEW_MATRIX,pSharedData->modelViewMatrix);

        //sets the invidual light source parameters
        //set the light position
        glLightfv(GL_LIGHT0, GL_POSITION, pos);

        //restore to the previous matrix - the one which was changed during the resize event
        glPopMatrix();

        //current matrix is changed again with the pop matrix operation
        //so  the update the shared data
        //glGetDoublev(GL_MODELVIEW_MATRIX,pSharedData->modelViewMatrix);

        //draw the loaded model
        pSharedData->drawModel(pSharedData->filePath);
        //drawModel();

        //disable lighting
        glDisable(GL_LIGHTING);
    }

    //push the copy of the current matrix again -
    //the one we got after the resize event
    glPushMatrix();

    //multiply the inverse matrix to the current model view matrix
    //come to  the camera position - eye coordinaates
    glMultMatrixf(pSharedData->invertedMatrix);


    //set the light with the position of the light
    glLightfv(GL_LIGHT0, GL_POSITION, pos);

    /* draw the axis and eye vector */

    //save the matrix that contains the camera coordinates
    glPushMatrix();

    glColor3ub(0, 0, 255);

    //draw the eye vector
    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, -1.0*length);
    glVertex3f(0.1, 0.0, -0.9*length);
    glVertex3f(-0.1, 0.0, -0.9*length);
    glVertex3f(0.0, 0.0, -1.0*length);
    glVertex3f(0.0, 0.1, -0.9*length);
    glVertex3f(0.0, -0.1, -0.9*length);
    glVertex3f(0.0, 0.0, -1.0*length);
    glEnd();

    glColor3ub(255, 255, 0);

    renderText(0.0, 0.0, -1.1*length,QChar('e'));
    glColor3ub(255, 0, 0);
    glScalef(0.4, 0.4, 0.4);

    drawAxes();

    //COME BACK to the eye coordinates
    glPopMatrix();

    //glGetDoublev(GL_MODELVIEW_MATRIX,pSharedData->modelViewMatrix);

    //retrieve the updated current projection matrix
    //glGetDoublev(GL_PROJECTION_MATRIX,projection);

    //calculate the inverse of the projection matrix
    inverted = invert(pSharedData->projectionMatrix,pSharedData->invertedMatrix);

//    if(inverted)
//        qDebug() << "Projection matrix is inverted";
//    else
//        qDebug() << "Projection matrix not inverted";

    glMultMatrixf(pSharedData->invertedMatrix);

    //glGetDoublev(GL_MODELVIEW_MATRIX,pSharedData->modelViewMatrix);

    /* draw the viewing frustum */

    //first the far plane is going to be drawn
    glColor3f(0.2, 0.2, 0.2);
    glBegin(GL_QUADS);
    glVertex3i(1, 1, 1);
    glVertex3i(-1, 1, 1);
    glVertex3i(-1, -1, 1);
    glVertex3i(1, -1, 1);
    glEnd();



    glColor3ub(128, 196, 128);
    glBegin(GL_LINES);
    glVertex3i(1, 1, -1);
    glVertex3i(1, 1, 1);
    glVertex3i(-1, 1, -1);
    glVertex3i(-1, 1, 1);
    glVertex3i(-1, -1, -1);
    glVertex3i(-1, -1, 1);
    glVertex3i(1, -1, -1);
    glVertex3i(1, -1, 1);
    glEnd();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(0.2, 0.2, 0.4, 0.5);

    glBegin(GL_QUADS);
    glVertex3i(1, 1, -1);
    glVertex3i(-1, 1, -1);
    glVertex3i(-1, -1, -1);
    glVertex3i(1, -1, -1);
    glEnd();
    glDisable(GL_BLEND);

    glPopMatrix();


    glPopMatrix();
}


