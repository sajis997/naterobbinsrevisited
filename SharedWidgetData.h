#ifndef SHAREDWIDGETDATA_H
#define SHAREDWIDGETDATA_H

#include <QObject>
#include <QtOpenGL/QGLContext>
#include <QtOpenGL/QGLFormat>
#include <QVector>
#include <QMap>

#include "GLToolkit.h"
#include "glm.h"

//class Cell;
//class XmlStreamReader;


class SharedWidgetData : public QObject
{
    Q_OBJECT

public:
    SharedWidgetData(  QObject *parent = 0 );

    ~SharedWidgetData();


    float getTranslationXFactor()
    {
        return translationVector[0];
    }

    float getTranslationYFactor() const
    {
        return translationVector[1];
    }

    float getTranslationZFactor() const
    {
        return translationVector[2];
    }

    float getRotationAngleFactor() const
    {
        return rotationVector[0];
    }

    float getRotationXFactor() const
    {
        return rotationVector[1];
    }

    float getRotationYFactor() const
    {
        return rotationVector[2];
    }

    float getRotationZFactor() const
    {
        return rotationVector[3];
    }

    float getScaleXFactor() const
    {
        return scaleVector[0];
    }

    float getScaleYFactor() const
    {
        return scaleVector[1];
    }

    float getScaleZFactor() const
    {
        return scaleVector[2];
    }

signals:

    void modelViewMatrixChanged();
    void projectionMatrixChanged();
    void inversionMatrixChanged();

    void parametersToBeReset();

    void cellMapContentChanged(double);


    //some more signals that
    //change positin and orientation
    void translationXFactorChanged();
    void translationYFactorChanged();
    void translationZFactorChanged();

    void rotationAngleFactorChanged();
    void rotationXFactorChanged();
    void rotationYFactorChanged();
    void rotationZFactorChanged();

    void scaleXFactorChanged();
    void scaleYFactorChanged();
    void scaleZFactorChanged();

    void cameraXPosChanged();
    void cameraYPosChanged();
    void cameraZPosChanged();

    void lookAtXPosChanged();
    void lookAtYPosChanged();
    void lookAtZPosChanged();

    void upVectorXfactorChanged();
    void upVectorYfactorChanged();
    void upVectorZfactorChanged();

    void leftOrthoFrameChanged();
    void rightOrthoFrameChanged();
    void topOrthoFrameChanged();
    void bottomOrthoFrameChanged();
    void nearOrthoFrameChanged();
    void farOrthoFrameChanged();


    void leftFrustumFrameChanged();
    void rightFrustumFrameChanged();
    void topFrustumFrameChanged();
    void bottomFrustumFrameChanged();
    void nearFrustumFrameChanged();
    void farFrustumFrameChanged();

    void fovPerspectiveFrameChanged();
    void aspectPerspectiveFrameChanged();
    void nearPerspectiveFrameChanged();
    void farPerspectiveFrameChanged();


    void projectionModeChanged(int);
    void projectionModeChanged();


public slots:

    void setModelViewMatrix(const GLfloat*);
    void setProjectionMatrix(const GLfloat*);
    void setInverseMatrix(const GLfloat*);

    void setSwapFlag(bool);
    void setWorldDrawFlag(bool);

    void setResetParametesFlag();

    void setTranslationXFactor(float);
    void setTranslationYFactor(float);
    void setTranslationZFactor(float);


    void setRotationAngleFactor(float);
    void setRotationXFactor(float);
    void setRotationYFactor(float);
    void setRotationZFactor(float);

    void setScaleXFactor(float);
    void setScaleYFactor(float);
    void setScaleZFactor(float);


    void setCameraXPos(float);
    void setCameraYPos(float);
    void setCameraZPos(float);

    void setLookAtXPos(float);
    void setLookAtYPos(float);
    void setLookAtZPos(float);


    void setUpVectorXfactor(float);
    void setUpVectorYfactor(float);
    void setUpVectorZfactor(float);

    void setOrthoMode();
    void setPerspectiveMode();
    void setFrustumMode();

    //void setProjectionMode(int);


    void setPerspectiveFOV(float);
    void setPerspectiveAspect(float);
    void setPerspectiveNear(float);
    void setPerspectiveFar(float);

    void setOrthoLeft(float);
    void setOrthoRight(float);
    void setOrthoTop(float);
    void setOrthoBottom(float);
    void setOrthoNear(float);
    void setOrthoFar(float);


    void setFrustumLeft(float);
    void setFrustumRight(float);
    void setFrustumTop(float);
    void setFrustumBottom(float);
    void setFrustumNear(float);
    void setFrustumFar(float);


    void printModelViewMatrix() const;
    void printProjectionMatrix() const;
    void printInverseMatrix() const;

    void printViewMatrix() const;


public:

    void drawModel(const QString&);


    GLfloat modelViewMatrix[16];
    GLfloat projectionMatrix[16];
    GLfloat invertedMatrix[16];

    GLfloat modelMatrix[16];
    GLfloat viewMatrix[16];

    GLfloat translationVector[3];
    GLfloat rotationVector[4];
    GLfloat scaleVector[3];

    GLfloat cameraPos[3];
    GLfloat lookAt[3];
    GLfloat upVector[3];

    // [0] - fov
    // [1] - aspect
    // [2] - near
    // [3] - far
    GLfloat perspectiveFrame[4];

    // [0] - left
    // [1] - right
    // [2] - top
    // [3] - bottom
    // [4] - near
    // [5] - far
    GLfloat orthoFrame[6];

    // [0] - left
    // [1] - right
    // [2] - top
    // [3] - bottom
    // [4] - near
    // [5] - far
    GLfloat frustumFrame[6];


    GLboolean world_draw;

    GLboolean swapped;

    bool resetParameters;

    GLMmodel *model;


    QString filePath;


    enum {
        PERSPECTIVE, FRUSTUM, ORTHO
    } mode;

};

#endif // SHAREDWIDGETDATA_H
