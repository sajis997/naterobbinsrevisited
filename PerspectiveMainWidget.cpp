#include "PerspectiveMainWidget.h"
#include "SharedWidgetData.h"

PerspectiveMainWidget::PerspectiveMainWidget(QWidget *parent)
    :QWidget(parent)
{
    setupUi(this);


    //set up some threshold value for the label
    fieldOfViewAngleValueLabel->setMinValue(1.0);
    fieldOfViewAngleValueLabel->setMaxValue(179.0);
    fieldOfViewAngleValueLabel->setStepValue(1.0);


    aspectRatioValueLabel->setMinValue(-3.0);
    aspectRatioValueLabel->setMaxValue(3.0);
    aspectRatioValueLabel->setStepValue(0.01);


    nearPlaneDistanceValueLabel->setMinValue(0.1);
    nearPlaneDistanceValueLabel->setMaxValue(10.0);
    nearPlaneDistanceValueLabel->setStepValue(0.05);


    farPlaneDistanceValueLabel->setMinValue(0.1);
    farPlaneDistanceValueLabel->setMaxValue(10.0);
    farPlaneDistanceValueLabel->setStepValue(0.05);


    eyeXPositionValueLabel->setMinValue(-5.0);
    eyeXPositionValueLabel->setMaxValue(5.0);
    eyeXPositionValueLabel->setStepValue(0.1);

    eyeYPositionValueLabel->setMinValue(-5.0);
    eyeYPositionValueLabel->setMaxValue(5.0);
    eyeYPositionValueLabel->setStepValue(0.1);

    eyeZPositionValueLabel->setMinValue(-5.0);
    eyeZPositionValueLabel->setMaxValue(5.0);
    eyeZPositionValueLabel->setStepValue(0.1);

    lookAtXPositionValueLabel->setMinValue(-5.0);
    lookAtXPositionValueLabel->setMaxValue(5.0);
    lookAtXPositionValueLabel->setStepValue(0.1);

    lookAtYPositionValueLabel->setMinValue(-5.0);
    lookAtYPositionValueLabel->setMaxValue(5.0);
    lookAtYPositionValueLabel->setStepValue(0.1);

    lookAtZPositionValueLabel->setMinValue(-5.0);
    lookAtZPositionValueLabel->setMaxValue(5.0);
    lookAtZPositionValueLabel->setStepValue(0.1);


    upVectorXPositionValueLabel->setMinValue(-2.0);
    upVectorXPositionValueLabel->setMaxValue(2.0);
    upVectorXPositionValueLabel->setStepValue(0.1);


    upVectorYPositionValueLabel->setMinValue(-2.0);
    upVectorYPositionValueLabel->setMaxValue(2.0);
    upVectorYPositionValueLabel->setStepValue(0.1);


    upVectorZPositionValueLabel->setMinValue(-2.0);
    upVectorZPositionValueLabel->setMaxValue(2.0);
    upVectorZPositionValueLabel->setStepValue(0.1);

}

PerspectiveMainWidget::~PerspectiveMainWidget()
{

}

void PerspectiveMainWidget::setSharedDataInstance(SharedWidgetData *data)
{
    if(data){
        pSharedData = data;
        createConnections();
    }
}


void PerspectiveMainWidget::createConnections()
{
    connect(fieldOfViewAngleValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setPerspectiveFOV(float)));
    connect(aspectRatioValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setPerspectiveAspect(float)));
    connect(nearPlaneDistanceValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setPerspectiveNear(float)));
    connect(farPlaneDistanceValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setPerspectiveFar(float)));

    connect(eyeXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraXPos(float)));
    connect(eyeYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraYPos(float)));
    connect(eyeZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraZPos(float)));

    connect(lookAtXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtXPos(float)));
    connect(lookAtYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtYPos(float)));
    connect(lookAtZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtZPos(float)));

    connect(upVectorXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorXfactor(float)));
    connect(upVectorYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorYfactor(float)));
    connect(upVectorZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorZfactor(float)));

}
