#ifndef GLTOOLKIT_H
#define GLTOOLKIT_H

#include <QtOpenGL/QGLContext>
#include <QtOpenGL/QGLFormat>
#include <QString>

#include <cmath>


//declare some static variables that will be used globally

//eye is set at position
//static GLfloat eye[3] = {0.0,0.0,2.0};

//and looking down the origin
//static GLfloat at[3] = {0.0,0.0,0.0};

//the up vector is towards the y - axis
//static GLfloat up[3] = {0.0,1.0,0.0};


GLboolean invert(GLfloat [],GLfloat []);

void identity(GLfloat []);

float normalize(float*);





#endif // GLTOOLKIT_H
