#ifndef FRUSTUMMAINWIDGET_H
#define FRUSTUMMAINWIDGET_H

#include <QWidget>

#include "ui_frustum.h"

class SharedWidgetData;

class FrustumMainWidget : public QWidget, private Ui::FrustumWidget
{
    Q_OBJECT

public:
    FrustumMainWidget(QWidget *parent = 0);
    ~FrustumMainWidget();

    void setSharedDataInstance(SharedWidgetData*);

    SharedWidgetData *getSharedDataInstance() const
    {
        return pSharedData;
    }

private:

    SharedWidgetData *pSharedData;

    void createConnections();
};



#endif // FRUSTUMMAINWIDGET_H
