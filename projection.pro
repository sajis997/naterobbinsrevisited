QT += opengl
HEADERS += \
    LabelCounter.h \
    FrustumMainWidget.h \
    OrthoMainWidget.h \
    PerspectiveMainWidget.h \
    SharedWidgetData.h \
    glWidgetWorldSpace.h \
    glWidgetScreenSpace.h \
    glm.h \
    ProjectionMainWindow.h \
    singleton.h \
    GLToolkit.h \
    GroupBoxCommandExtendedBox.h

SOURCES += \
    LabelCounter.cpp \
    FrustumMainWidget.cpp \
    OrthoMainWidget.cpp \
    PerspectiveMainWidget.cpp \
    SharedWidgetData.cpp \
    glWidgetWorldSpace.cpp \
    glWidgetScreenSpace.cpp \
    glm.cpp \
    ProjectionMainWindow.cpp \
    main.cpp \
    GLToolkit.cpp \
    GroupBoxCommandExtendedBox.cpp

FORMS += \
    projection.ui \
    perspective.ui \
    ortho.ui \
    frustum.ui
