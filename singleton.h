
#ifndef TGT_SINGLETON_H
#define TGT_SINGLETON_H

#include <QtCore/QtDebug>
#include <QtCore/QtGlobal>

//#include <iostream>

//#include "tgt/config.h"
//#include "tgt/assert.h"

//namespace tgt {

/**
    This class helps to build the singleton design pattern.
    Here you have full control over construction and deconstruction
    of the object.
*/

template<class T>
class Singleton
{
public:
    /**
     * Init the actual singleton.
     * Must be called BEFORE the class is used, like this:
     *
     * Singleton\<TextureManager\>::init(new TextureManager());
     */
    static void init(T* singletonClass)
    {
        //tgtAssert( !singletonClass_, "singletonClass_ has already been intitialized." );
        Q_ASSERT_X(!singletonClass_,"Instantiation","singletonClass_ has already been intitialized.");
        singletonClass_ = singletonClass;
    }

    /**
     * Deinit the actual singleton.
     * Must be done at last.
     */
    static void deinit()
    {
        //tgtAssert( singletonClass_ != 0, "singletonClass_ has already been deintitialized." );
        Q_ASSERT_X(singletonClass_ != 0,"de-initialization","singletonClass_ has already been deintitialized.");
        delete singletonClass_;
        singletonClass_ = 0;
    }

    /**
     * Get Pointer of the actual class
     * @return Pointer of the actual class
     */
    static T* getPtr()
    {
        //tgtAssert( singletonClass_ != 0, "singletonClass_ has not been intitialized." );
        Q_ASSERT_X(singletonClass_ != 0,"returning class pointer", "singletonClass_ has not been intitialized.");
        return singletonClass_;
    }

    /**
     * Get reference of the actual class
     * @return reference of the actual class
    */
    static T& getRef()
    {
        //tgtAssert( singletonClass_ != 0 , "singletonClass_ has not been intitialized." );
        Q_ASSERT_X(singletonClass_ != 0,"returning class pointer", "singletonClass_ has not been intitialized.");
        return *singletonClass_;
    }

    /**
     * Has the actual class been inited?
     */
    static bool isInited()
    {
        return (singletonClass_ != 0);
    }

private:
    static T* singletonClass_;
};

/// init static pointers with 0
template<class T>

T* Singleton<T>::singletonClass_ = 0;

//} // namespace

#endif // TGT_SINGLETON_H
