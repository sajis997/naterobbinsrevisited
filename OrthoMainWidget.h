#ifndef ORTHOMAINWIDGET_H
#define ORTHOMAINWIDGET_H

#endif // ORTHOMAINWIDGET_H


#include <QWidget>
#include "ui_ortho.h"

class SharedWidgetData;

class OrthoMainWidget : public QWidget , private Ui::OrthoWidget
{
    Q_OBJECT

public:
    OrthoMainWidget(QWidget *parent = 0 );
    ~OrthoMainWidget();

    void setSharedDataInstance(SharedWidgetData*);

    SharedWidgetData *getSharedDataInstance() const
    {
        return pSharedData;
    }

private:

    void createConnections();

    SharedWidgetData *pSharedData;
};
