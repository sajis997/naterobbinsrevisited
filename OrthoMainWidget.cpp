#include "OrthoMainWidget.h"
#include "SharedWidgetData.h"


OrthoMainWidget::OrthoMainWidget(QWidget *parent)
    :QWidget(parent)
{
    setupUi(this);


    //set up some threshold value for the label
    leftValueLabelOrtho->setMinValue(-10);
    leftValueLabelOrtho->setMaxValue(10);
    leftValueLabelOrtho->setStepValue(0.1);


    rightValueLabelOrtho->setMinValue(-10);
    rightValueLabelOrtho->setMaxValue(10);
    rightValueLabelOrtho->setStepValue(0.1);


    bottomValueLabelOrtho->setMinValue(-10);
    bottomValueLabelOrtho->setMaxValue(10);
    bottomValueLabelOrtho->setStepValue(0.1);


    topValueLabelOrtho->setMinValue(-10);
    topValueLabelOrtho->setMaxValue(10);
    topValueLabelOrtho->setStepValue(0.1);


    nearValueLabelOrtho->setMinValue(-5.0);
    nearValueLabelOrtho->setMaxValue(5.0);
    nearValueLabelOrtho->setStepValue(0.01);


    farValueLabelOrtho->setMinValue(-5.0);
    farValueLabelOrtho->setMaxValue(5.0);
    farValueLabelOrtho->setStepValue(0.01);

    eyeXPositionValueLabel->setMinValue(-5.0);
    eyeXPositionValueLabel->setMaxValue(5.0);
    eyeXPositionValueLabel->setStepValue(0.1);

    eyeYPositionValueLabel->setMinValue(-5.0);
    eyeYPositionValueLabel->setMaxValue(5.0);
    eyeYPositionValueLabel->setStepValue(0.1);

    eyeZPositionValueLabel->setMinValue(-5.0);
    eyeZPositionValueLabel->setMaxValue(5.0);
    eyeZPositionValueLabel->setStepValue(0.1);

    lookAtXPositionValueLabel->setMinValue(-5.0);
    lookAtXPositionValueLabel->setMaxValue(5.0);
    lookAtXPositionValueLabel->setStepValue(0.1);

    lookAtYPositionValueLabel->setMinValue(-5.0);
    lookAtYPositionValueLabel->setMaxValue(5.0);
    lookAtYPositionValueLabel->setStepValue(0.1);

    lookAtZPositionValueLabel->setMinValue(-5.0);
    lookAtZPositionValueLabel->setMaxValue(5.0);
    lookAtZPositionValueLabel->setStepValue(0.1);


    upVectorXPositionValueLabel->setMinValue(-2.0);
    upVectorXPositionValueLabel->setMaxValue(2.0);
    upVectorXPositionValueLabel->setStepValue(0.1);


    upVectorYPositionValueLabel->setMinValue(-2.0);
    upVectorYPositionValueLabel->setMaxValue(2.0);
    upVectorYPositionValueLabel->setStepValue(0.1);


    upVectorZPositionValueLabel->setMinValue(-2.0);
    upVectorZPositionValueLabel->setMaxValue(2.0);
    upVectorZPositionValueLabel->setStepValue(0.1);

}

void OrthoMainWidget::setSharedDataInstance(SharedWidgetData *data)
{
    if(data)
        pSharedData = data;

    createConnections();
}


void OrthoMainWidget::createConnections()
{
    connect(leftValueLabelOrtho,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setOrthoLeft(float)));
    connect(rightValueLabelOrtho,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setOrthoRight(float)));
    connect(bottomValueLabelOrtho,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setOrthoBottom(float)));
    connect(topValueLabelOrtho,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setOrthoTop(float)));
    connect(nearValueLabelOrtho,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setOrthoNear(float)));
    connect(farValueLabelOrtho,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setOrthoFar(float)));

    connect(eyeXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraXPos(float)));
    connect(eyeYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraYPos(float)));
    connect(eyeZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setCameraZPos(float)));

    connect(lookAtXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtXPos(float)));
    connect(lookAtYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtYPos(float)));
    connect(lookAtZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setLookAtZPos(float)));

    connect(upVectorXPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorXfactor(float)));
    connect(upVectorYPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorYfactor(float)));
    connect(upVectorZPositionValueLabel,SIGNAL(labelValueChanged(float)),pSharedData,SLOT(setUpVectorZfactor(float)));
}


OrthoMainWidget::~OrthoMainWidget()
{

}
