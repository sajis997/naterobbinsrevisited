#ifndef PROJECTIONMAINWINDOW_H
#define PROJECTIONMAINWINDOW_H

#include <QMainWindow>

#include "ui_projection.h"
#include "FrustumMainWidget.h"
#include "OrthoMainWidget.h"
#include "PerspectiveMainWidget.h"

class QStackedLayout;
class QGridLayout;


class ProjectionMainWindow : public QMainWindow , private Ui::ProjectionWindow
{
    Q_OBJECT
public:
    ProjectionMainWindow(QWidget *parent = 0);
    ~ProjectionMainWindow();

public slots:
    //void setProjectionModeWidget();
    //void setPerspectiveModeWidget();
    //void setFrustumModeWidget();

private slots:
    void updateGLWidgets();
    void updateProjectionMatrixLabelValues();
    void updateModelMatrixLabelValues();
    void updateViewMatrixLabelValues();
    void updateModelViewMatrixLabelValues();

private:

    OrthoMainWidget *stackViewWidgetOrtho;
    PerspectiveMainWidget *stackViewWidgetPerspective;
    FrustumMainWidget *stackViewWidgetFrustum;


    QStackedLayout *stackLayout;
    QGridLayout *gridLayout;

    QVBoxLayout *verticalLayoutCustom;
    QHBoxLayout *horizontalLayoutCustom;
};


#endif // PROJECTIONMAINWINDOW_H
