#include <QtGui>
#include "LabelCounter.h"

LabelCounter::LabelCounter(QWidget *parent)
    : QLabel(parent), oldMousePos(0),step(0.01), minValue(0.0),maxValue(0.0)
{

}
LabelCounter::~LabelCounter()
{

}


void LabelCounter::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) {

        //save the mouse y - Position
        oldMousePos = event->y();

        //get the current font
        QFont labelFont = font();

        //preserve the current font
        initialFont = labelFont;

        labelFont.setBold(true);
        labelFont.setItalic(true);


        setFont(labelFont);

        //qDebug() << "Mouse pressed and attributes changed";
    }
}

void LabelCounter::mouseMoveEvent(QMouseEvent *event)
{
    float num;
    //QVariant numV;

    if(event->buttons() & Qt::LeftButton) {

        int mouseReleasePos = event->y();

        if(mouseReleasePos < oldMousePos) {

            num = text().toFloat() + step;


            if(num <= maxValue) {
                setNum(num);
            }

            emit labelValueChanged(num);

            //qDebug() << "Label value changed signal sent";

        }
        else if(mouseReleasePos > oldMousePos) {

            num = text().toFloat() - step;

            if(num >= minValue) {

                setNum(num);
            }

            emit labelValueChanged(num);

            //qDebug() << "Label value changed signal sent";

        }
    }
}

void LabelCounter::mouseReleaseEvent(QMouseEvent *event )
{
    if(event->button() == Qt::LeftButton) {
        setFont(initialFont);

        //qDebug() << "Re-instating the initial label";
    }
}
