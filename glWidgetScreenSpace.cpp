#include <QtGui>
#include <QtOpenGL>


#include "SharedWidgetData.h"
#include "glWidgetScreenSpace.h"



GLWidgetScreenSpace::GLWidgetScreenSpace(QWidget *parent)
    :QGLWidget(parent)
{
    setFormat(QGL::DoubleBuffer | QGL::DepthBuffer);


    screenidgetSpaceHeight = 0;
    screenWidgetSpaceWidth = 0;

    pSharedData = NULL;
}



void GLWidgetScreenSpace::setSharedDataInstance(SharedWidgetData *sharedData)
{
    if(sharedData)
        pSharedData = sharedData;


    //the most important of all is the initializing the shared data pointer
    //now we are ready to create some actions
    createActions();
}


void GLWidgetScreenSpace::createActions()
{
    loadModelAction = new QAction(tr("&Load model..."),this);
    loadModelAction->setStatusTip(tr("Load model"));

    connect(loadModelAction,SIGNAL(triggered()),this,SLOT(loadModel()));
}


void GLWidgetScreenSpace::loadModel()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open Object Model"),"data/",tr("Object Model Files (*.obj)"));

    if( !fileName.isEmpty()) {
        //check if the previous model is still loaded or not
        if( pSharedData->model) {
            //model is still pointing to something
            //so remove it first and then load another model
            glmDelete(pSharedData->model);
            pSharedData->model = NULL;
        }

        pSharedData->filePath = fileName;
        pSharedData->drawModel(fileName);
    }

    updateGL();
}

void GLWidgetScreenSpace::initializeGL()
{
    /*
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable(GL_CULL_FACE);
    */
}

void GLWidgetScreenSpace::screenReshape(int width,int height)
{

    //opengl commands is outside the paintGL and resizeGL
    //so we have to make it the current context
    makeCurrent();

    glViewport(0,0,width,height);

    //change the matrix mode to projection matrix
    glMatrixMode(GL_PROJECTION);
    //make the projection matirx an identity matrix
    glLoadIdentity();

    //projection matrix will be changed based on the mode of projection
    if(pSharedData->mode == SharedWidgetData::PERSPECTIVE) {
        gluPerspective(pSharedData->perspectiveFrame[0],pSharedData->perspectiveFrame[1],
                       pSharedData->perspectiveFrame[2],pSharedData->perspectiveFrame[3]);
    }
    else if(pSharedData->mode == SharedWidgetData::ORTHO) {
        glOrtho(pSharedData->orthoFrame[0],pSharedData->orthoFrame[1],
                pSharedData->orthoFrame[2],pSharedData->orthoFrame[3],
                pSharedData->orthoFrame[4],pSharedData->orthoFrame[5]);
    }
    else if(pSharedData->mode == SharedWidgetData::FRUSTUM) {
        glFrustum(pSharedData->frustumFrame[0],pSharedData->frustumFrame[1],
                  pSharedData->frustumFrame[2],pSharedData->frustumFrame[3],
                  pSharedData->frustumFrame[4],pSharedData->frustumFrame[5]);
    }


    //get the projection matrix and set it in the shared data's projection matrix
    glGetFloatv(GL_PROJECTION_MATRIX,pSharedData->projectionMatrix);

    //projection matrix is altered, and so the signal is emitted
    emit projectionScreenMatrixChanged();


    //change the matrix mode to the model view matrix
    glMatrixMode(GL_MODELVIEW);
    //make the model view matrix is an identity matrix
    glLoadIdentity();


    //the inverse of the camera positon/orientation is set into the model-view matrix
    gluLookAt(pSharedData->cameraPos[0],pSharedData->cameraPos[1],pSharedData->cameraPos[2],
              pSharedData->lookAt[0],pSharedData->lookAt[1],pSharedData->lookAt[2],
              pSharedData->upVector[0],pSharedData->upVector[1],pSharedData->upVector[2]);

    //here the view matrix is changed ,so we save the data in the shared data's matrix
    glGetFloatv(GL_MODELVIEW_MATRIX,pSharedData->viewMatrix);


    emit viewMatrixScreenSpaceChanged();

    //make the model view matrix the identity one again
    glLoadIdentity();

    //save the matrix
    glGetFloatv(GL_MODELVIEW_MATRIX,pSharedData->modelMatrix);


    //emit the signal
    emit modelMatrixScreenSpaceChanged();


    glLoadMatrixf(pSharedData->viewMatrix);
    glMultMatrixf(pSharedData->modelMatrix);


    //save the model view matrix inside the shared data
    glGetFloatv(GL_MODELVIEW_MATRIX,pSharedData->modelViewMatrix);

    //emit a signal in the SharedWidgetData
    emit modelviewScreenMatrixChanged();

    glClearColor(0.2,0.2,0.2,0.0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}


void GLWidgetScreenSpace::resizeGL(int width,int height)
{
    screenidgetSpaceHeight = height;
    screenWidgetSpaceWidth = width;

    screenReshape(screenWidgetSpaceWidth,screenidgetSpaceHeight);
}

void GLWidgetScreenSpace::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    pSharedData->drawModel(pSharedData->filePath);

}


void GLWidgetScreenSpace::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

    menu.addAction(loadModelAction);

    menu.exec(event->globalPos());
}
