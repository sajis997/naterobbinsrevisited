#ifndef GLWIDGETWORLDSPACE_H
#define GLWIDGETWORLDSPACE_H

#include <QGLWidget>

#include "GLToolkit.h"
#include "glm.h"


//all system independant opengl commads should reside here

class SharedWidgetData;

class GLWidgetWorldSpace : public QGLWidget
{
    Q_OBJECT
public:
    GLWidgetWorldSpace(QWidget *parent = 0);

    void setSharedDataInstance(SharedWidgetData*);

    SharedWidgetData *getSharedDataInstance() const
    {
        return pSharedData;
    }

    int getWorldWidgetSpaceWidth() const
    {
        return worldWidgetSpaceWidth;
    }

    int getWorldWidgetSpaceHeight() const
    {
        return worldWidgetSpaceHeight;
    }


    void worldReshape(int,int);


protected:
    void initializeGL();
    void resizeGL(int,int);
    void paintGL();
    void mousePressEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent*);

    void contextMenuEvent(QContextMenuEvent *event);

signals:

    void xRotationChanged(int);
    void yRotationChanged(int);
    void zRotationChanged(int);

private slots:

//    void setModelviewWorldMatrix(GLdouble*);
//    void setProjectionWorldMatrix(GLdouble*);
//    void setInverseWorldMatrix(GLdouble*);

    void setXRotation(int);
    void setYRotation(int);
    void setZRotation(int);

    void resetWorldScene();

private:

    void createActions();

    void drawAxes();

    void normalizeAngle(int*);


    SharedWidgetData *pSharedData;

    int worldWidgetSpaceWidth;
    int worldWidgetSpaceHeight;

    QPoint lastPos;

    QAction *resetWorldAction;

    int xRot;
    int yRot;
    int zRot;

};


#endif // GLWIDGETWORLDSPACE_H
