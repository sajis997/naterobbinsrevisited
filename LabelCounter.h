#ifndef LABELCOUNTER_H
#define LABELCOUNTER_H

#include <QLabel>

class LabelCounter : public QLabel
{
    Q_OBJECT

public:
    LabelCounter(QWidget *parent = 0);

    ~LabelCounter();

    float getMinValue() const
    {
        return minValue;
    }

    float getMaxValue() const
    {
        return maxValue;
    }


    float getStepValue() const
    {
        return step;
    }

    void setMinValue(float val)
    {
        minValue = val;
    }

    void setMaxValue(float val)
    {
        maxValue = val;
    }


    void setStepValue(float s)
    {
        step = s;
    }


signals:
    void labelValueChanged(float);

protected:

    void mousePressEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);


private:

    //preserve the designer font
    QFont initialFont;

    int oldMousePos;

    float step;

    float minValue;
    float maxValue;


};

#endif // LABELCOUNTER_H
