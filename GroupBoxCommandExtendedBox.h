#ifndef GROUPBOXCOMMANDEXTENDEDBOX_H
#define GROUPBOXCOMMANDEXTENDEDBOX_H

#include <QGroupBox>


class QAction;
class QContextMenuEvent;
class QActionGroup;
class SharedWidgetData;

class GroupBoxCommandExtendedBox : public QGroupBox
{
    Q_OBJECT

public:
    GroupBoxCommandExtendedBox(QWidget *parent = 0);

    void contextMenuEvent(QContextMenuEvent *);


    void setSharedDataInstanceGroupBox(SharedWidgetData*);

    SharedWidgetData *getSharedDataInstanceGroupBox() const
    {
        return pSharedData;
    }

signals:

    void triggerForOrtho();
    void triggerForPerspective();
    void triggerForFrustum();

private slots:
    void resetParameters();
    //void swapWidgets();

private:

    void createActions();

    QAction *resetAction;
    QAction *swapAction;



    QAction *orthoAction;
    QAction *perspectiveAction;
    QAction *frustumAction;

    SharedWidgetData *pSharedData;

};

#endif // GROUPBOXCOMMANDEXTENDEDBOX_H
