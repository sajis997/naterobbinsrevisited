#ifndef GLWIDGETSCREENSPACE_H
#define GLWIDGETSCREENSPACE_H

#include <QGLWidget>

#include "GLToolkit.h"
#include "glm.h"

class QContextMenuEvent;
class QAction;

class SharedWidgetData;


class GLWidgetScreenSpace : public QGLWidget
{
    Q_OBJECT

public:
    GLWidgetScreenSpace(QWidget *parent = 0);


    void setSharedDataInstance(SharedWidgetData*);


    SharedWidgetData *getSharedDataInstance() const
    {
        return pSharedData;
    }

    int getScreenWidgetSpaceWidth() const
    {
        return screenWidgetSpaceWidth;
    }

    int getScreenWidgetSpaceHeight() const
    {
        return screenidgetSpaceHeight;
    }


    void screenReshape(int,int);


protected:
    void initializeGL();
    void resizeGL(int,int);
    void paintGL();


    void contextMenuEvent(QContextMenuEvent *event);
signals:
    void modelviewScreenMatrixChanged();
    void viewMatrixScreenSpaceChanged();
    void modelMatrixScreenSpaceChanged();

    void projectionScreenMatrixChanged();
//    void projectionScreenMatrixChanged(GLdouble*);
//    void inverseScreenMatrixChanged(GLdouble*);

private slots:
//    void setModelviewScreenMatrix(GLdouble*);
//    void setProjectionScreenMatrix(GLdouble*);
//    void setInverseScreenMatrix(GLdouble*);
    void loadModel();

private:

    void createActions();



    QAction *loadModelAction;

    SharedWidgetData *pSharedData;

    int screenWidgetSpaceWidth;
    int screenidgetSpaceHeight;


};


#endif // GLWIDGETSCREENSPACE_H
