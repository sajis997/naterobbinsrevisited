#include "GLToolkit.h"

float normalize(float *v)
{
    float length;

    length = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
    v[0] /= length;
    v[1] /= length;
    v[2] /= length;

    return length;
}

void identity(GLfloat identity[16])
{
    identity[0+4*0] = 1; identity[0+4*1] = 0; identity[0+4*2] = 0; identity[0+4*3] = 0;
    identity[1+4*0] = 0; identity[1+4*1] = 1; identity[1+4*2] = 0; identity[1+4*3] = 0;
    identity[2+4*0] = 0; identity[2+4*1] = 0; identity[2+4*2] = 1; identity[2+4*3] = 0;
    identity[3+4*0] = 0; identity[3+4*1] = 0; identity[3+4*2] = 0; identity[3+4*3] = 1;
}

GLboolean invert(GLfloat src[16],GLfloat inverse[16])
{
    float t;
    int i, j, k, swap;
    GLfloat tmp[4][4];

    identity(inverse);

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            tmp[i][j] = src[i*4+j];
        }
    }

    for (i = 0; i < 4; i++) {
        /* look for largest element in column. */
        swap = i;
        for (j = i + 1; j < 4; j++) {
            if (fabs(tmp[j][i]) > fabs(tmp[i][i])) {
                swap = j;
            }
        }

        if (swap != i) {
            /* swap rows. */
            for (k = 0; k < 4; k++) {
                t = tmp[i][k];
                tmp[i][k] = tmp[swap][k];
                tmp[swap][k] = t;

                t = inverse[i*4+k];
                inverse[i*4+k] = inverse[swap*4+k];
                inverse[swap*4+k] = t;
            }
        }

        if (tmp[i][i] == 0) {
        /* no non-zero pivot.  the matrix is singular, which
        shouldn't happen.  This means the user gave us a bad
            matrix. */
            return GL_FALSE;
        }

        t = tmp[i][i];
        for (k = 0; k < 4; k++) {
            tmp[i][k] /= t;
            inverse[i*4+k] /= t;
        }
        for (j = 0; j < 4; j++) {
            if (j != i) {
                t = tmp[j][i];
                for (k = 0; k < 4; k++) {
                    tmp[j][k] -= tmp[i][k]*t;
                    inverse[j*4+k] -= inverse[i*4+k]*t;
                }
            }
        }
    }

    return GL_TRUE;
}


