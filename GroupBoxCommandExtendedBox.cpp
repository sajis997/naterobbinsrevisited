#include <QMenu>
#include <QAction>
#include <QDebug>
#include <QtGui/QContextMenuEvent>


#include "GroupBoxCommandExtendedBox.h"
#include "SharedWidgetData.h"

GroupBoxCommandExtendedBox::GroupBoxCommandExtendedBox(QWidget *parent)
    :QGroupBox(parent)
{

}

void GroupBoxCommandExtendedBox::setSharedDataInstanceGroupBox(SharedWidgetData *sharedData)
{
   Q_ASSERT(sharedData);
   pSharedData = sharedData;


   //shared data pointer is retrieved
   //so it is time to create the necessary actions
   createActions();
}


void GroupBoxCommandExtendedBox::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

    menu.addAction(resetAction);
    menu.addAction(orthoAction);
    menu.addAction(perspectiveAction);
    menu.addAction(frustumAction);

    menu.exec(event->globalPos());
}


void GroupBoxCommandExtendedBox::createActions()
{
    resetAction = new QAction(tr("Reset the command parameters"),this);
    resetAction->setStatusTip(tr("Reset the command parameters to initial values"));
    connect(resetAction,SIGNAL(triggered()),pSharedData,SLOT(setResetParametesFlag()));


    //create a action group
    QActionGroup *groupAction  = new QActionGroup(this);


    orthoAction = new QAction(tr("gl&Ortho"),groupAction);
    orthoAction->setStatusTip(tr("Load the orthographic projected view volume"));
    orthoAction->setCheckable(true);
    //set the ortho option from the context menu
    connect(orthoAction,SIGNAL(triggered()),pSharedData,SLOT(setOrthoMode()));

    perspectiveAction = new QAction(tr("gl&Perspective"),groupAction);
    perspectiveAction->setStatusTip(tr("Load the perspective projected view volume"));
    perspectiveAction->setCheckable(true);
    perspectiveAction->setChecked(true);
    connect(perspectiveAction,SIGNAL(triggered()),pSharedData,SLOT(setPerspectiveMode()));


    frustumAction = new QAction(tr("gl&Frustum"),groupAction);
    frustumAction->setStatusTip(tr("Load the frustum projected view volume"));
    frustumAction->setCheckable(true);
    connect(frustumAction,SIGNAL(triggered()),pSharedData,SLOT(setFrustumMode()));
}


void GroupBoxCommandExtendedBox::resetParameters()
{

}
